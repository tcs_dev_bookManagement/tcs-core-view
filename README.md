# 概要

TCS-COREの画面を作成する。  
TCS-CORE→[https://gitlab.com/tcs_dev_bookManagement/tcs-core](https://gitlab.com/tcs_dev_bookManagement/tcs-core)

## ドキュメント

- [使用技術・ツール](https://gitlab.com/tcs_dev_bookManagement/tcs-core-view/wikis/1.%E4%BD%BF%E7%94%A8%E6%8A%80%E8%A1%93%E3%83%BB%E3%83%84%E3%83%BC%E3%83%AB)
- [対応ブラウザ](https://gitlab.com/tcs_dev_bookManagement/tcs-core-view/wikis/2.%E5%AF%BE%E5%BF%9C%E3%83%96%E3%83%A9%E3%82%A6%E3%82%B6)
- [画面案](https://gitlab.com/tcs_dev_bookManagement/tcs-core-view/wikis/3.%E7%94%BB%E9%9D%A2%E6%A1%88)
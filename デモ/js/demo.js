/**
 * 画面がロードされた後に呼ばれます。
 * @param event
 * @returns
 */
document.addEventListener("DOMContentLoaded", function(event) {
    /**
     * vuejsのデータ初期化
     */
	var app = new Vue({
		/** アプリのID */
		el : '#app',
        /** データ */
		data : {
			message : '文字を入力してください。',
			items :new Array()
		},
        /** イベント処理 */
		methods : {
			/**
			 * 配列に値を追加する
			 */
			addList : (event) => {
				var id = Math.ceil(Math.random()*100);
				app.items.push(createItem(id));
			},
			/**
			 * 配列から値を削除する
			 */
			delList : (event) => {
				if(0 < app.items.length){
					app.items.pop() ;
				}
			}
		}
	});
});

/**
 * 配列に追加するデータを返す
 * @return
 */
function createItem(id){
	var item = {
        id:id,
        name:"田中",
        tel:"033-3333-3333"
		};
	return item;
}